import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'blog')

app = Celery('blog')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()


@app.task(bind=True)
def s(self):
    print('Request: {0!r}'.format(self.request))