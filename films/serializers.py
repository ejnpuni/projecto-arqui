from. models import Films, Customer, Ticket
from rest_framework import serializers

class FilmsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Films
        fields = ('movie_name', 'movie_year', 'movie_rating')
        
class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ('name','lastname', 'email', 'cellphone')
        
class TicketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticket
        fields = ( 'customer_id', 'film_id', 'seat', 'date', 'time', 'price', 'quantity')
        
    
    