import email
from itertools import count
from django.db import models

class Films(models.Model):

    movie_name = models.CharField(max_length=100)
    movie_year = models.CharField(max_length=10)
    movie_rating =models.CharField(max_length=10)
    
    def __str__(self):
        return self.movie_name
    
    
class Customer(models.Model):
    
    name = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    cellphone = models.CharField(max_length=10)
    
    def __str__(self):
        return self.name

class Ticket(models.Model):
    
    customer_id = models.ForeignKey(Customer, on_delete=models.CASCADE)
    film_id = models.ForeignKey(Films, on_delete=models.CASCADE)
    seat = models.CharField(max_length=10)
    date = models.DateField()
    time = models.TimeField()
    price = models.CharField(max_length=10)
    quantity = models.IntegerField()
    
    def __str__(self):
        return self.customer_id.name

    
