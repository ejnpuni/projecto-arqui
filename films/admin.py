from django.contrib import admin
from .models import Films, Customer, Ticket


admin.site.register(Films)
admin.site.register(Customer)
admin.site. register(Ticket)