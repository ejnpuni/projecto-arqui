from pprint import pprint
from celery import shared_task
from bs4 import BeautifulSoup
import requests
import pandas as pd
import csv


url = 'https://www.ecartelera.com/listas/mejores-peliculas/'
page = requests.get(url)
soup = BeautifulSoup(page.content, 'html.parser')

@shared_task
def get_ratings():
    movie_rating = soup.find_all('div', class_='nota verde')
    movie_ratings = list()
    count = 0
    for i in movie_rating:
        if count < 90:
            movie_ratings.append(i.text)
        else:
            break
        count += 1
    return movie_ratings

@shared_task
def get_years():
    movie_year = soup.find_all('div', class_='year')
    movie_years = list()
    count = 0
    for i in movie_year:
        if count < 90:
            movie_years.append(i.text)
        else:
            break
        count += 1
    return movie_years

@shared_task
def get_names():
    movie_name = soup.find_all('p', class_='title')
    movie_names = list()
    count = 0
    for i in movie_name:
        if count < 90:
            movie_names.append(i.text)
        else:
            break
        count += 1
    return movie_names