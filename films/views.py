from django.shortcuts import render
from .task import get_names, get_ratings, get_years
from films.models import Films, Customer, Ticket
from rest_framework import viewsets
from .serializers import FilmsSerializer, CustomerSerializer, TicketSerializer
#Serializers

class FilmsViewSet(viewsets.ModelViewSet):
    queryset = Films.objects.all()
    serializer_class = FilmsSerializer
    #permission_classes = [permissions.IsAuthenticated]
    
class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    
class TicketViewSet(viewsets.ModelViewSet):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer

def home(request):
    
    names = get_names()
    years = get_years()
    ratings = get_ratings()
    
    for i in range(len(names)):
        Films.objects.create(
            movie_name=names[i],
            movie_year=years[i],
            movie_rating=ratings[i]
        )
        
    for i in range(len(names)):
        Customer.objects.create(
            name=names[i],
            lastname=names[i],
            email=names[i],
            cellphone=names[i]
        )
    for i in range(len(names)):
        Ticket.objects.create(
            customer=Customer.objects.get(name=names[i]),
            film=Films.objects.get(movie_name=names[i]),
            date=names[i],
            time=names[i],
            seat=names[i],
            price=names[i]
        )
    
    return render(request, 'home.html')