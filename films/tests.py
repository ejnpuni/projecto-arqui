from django.test import TestCase
import pytest

from .models import Customer

class CustomerTestCase(TestCase):

    def test_user_creation(self):
        user = Customer.objects.create(
                name = 'cosa',
                lastname = 'otracosa',
                email = 'cosa@gmail.com',
                cellphone = '123456789'
                
            )
        self.assertEqual(Customer.objects.all().count(), 1)

class CustomerTestCase(TestCase):
    
    def test_user_creation(self):
        user = Customer.objects.superuser(
                name = 'cosa',
                lastname = 'otracosa',
                email = 'cosa@gmail.com',
                cellphone = '123456789'
                
            )
        self.assertEqual(Customer.objects.all().count(), 1)
