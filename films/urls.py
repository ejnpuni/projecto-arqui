from django.urls import path
from rest_framework import routers

from films.views import FilmsViewSet, CustomerViewSet, TicketViewSet, home

router = routers.DefaultRouter()
router.register('films', FilmsViewSet)
router.register('customers', CustomerViewSet)
router.register('tickets', TicketViewSet)

urlpatterns = [
    path('home/', home),
] + router.urls